# Exchange

Ce repository est un fork d'un projet réalisé par Simplon Roanne.

J'ai contribué a détecter une faille Xss stocké, et ainsi réaliser un test
cypress.

http://simplon-exchange.help


## Installation

- `git clone https://gitlab.com/simplon-roanne/exchange`
- `cd exchange`
- `composer install`
-  Copier .env.example vers .env et remplir les informations DB
- `php artisan migrate`

## Mise à jour du projet

- `php artisan migrate:fresh`